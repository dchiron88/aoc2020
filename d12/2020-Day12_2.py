# -*-coding:Latin-1 -*
import os
from math import *
    
#calcul des nouvelles positions du bateau et de l'arriv�e en fonction de l'ancienne position et de l'instruction donn�e
def calcpos(pos,instr):
    EWs=pos[0] #position du bateau suivant la direction Est/Ouest (+ pour l'Est et - pour l'Ouest)
    NSs=pos[1] #position du bateau suivant la direction Nord/Sud (+ pour le Nord et - pour le Sud)
    EWw=pos[2] #position de l'arriv�e suivant la direction Est/Ouest (+ pour l'Est et - pour l'Ouest)
    NSw=pos[3] #position de l'arriv�e suivant la direction Nord/Sud (+ pour le Nord et - pour le Sud)
    a=instr[0] #action
    v=int(instr[1:]) #valeur
    if a=="N":
        NSw+=v
    elif a=="S":
        NSw-=v
    elif a=="E":
        EWw+=v
    elif a=="W":
        EWw-=v
    elif a=="L":
        x=EWw
        y=NSw
        EWw=x*int(cos(v*pi/180))-y*int(sin(v*pi/180))
        NSw=x*int(sin(v*pi/180))+y*int(cos(v*pi/180))
    elif a=="R":
        x=EWw
        y=NSw
        EWw=x*int(cos(v*pi/180))+y*int(sin(v*pi/180))
        NSw=-x*int(sin(v*pi/180))+y*int(cos(v*pi/180))
    elif a=="F":
        EWs+=v*EWw
        NSs+=v*NSw
    else:
        print("Probl�me dans l'instruction")
    return [int(EWs),int(NSs),int(EWw),int(NSw)]

def calc(l):
    oldpos=[0,0,10,1]
    for elt in l:
        newpos=calcpos(oldpos,elt)
        #print(elt,"EW ship =",newpos[0],"NS ship =",newpos[1],"EW waypoint =",newpos[2],"NS waypoint =",newpos[3])
        oldpos.clear()
        oldpos.extend(newpos)
        newpos.clear()
    return int(abs(oldpos[0])+abs(oldpos[1]))
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration des groupes dans le fichier
    print(calc(liste))
    f.close()

main()
