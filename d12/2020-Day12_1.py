# -*-coding:Latin-1 -*
import os
    
#calcul de la nouvelle position en fonction de l'ancienne position et de l'instruction donn�e
def calcpos(oldpos,instr):
    EW=oldpos[0] #position suivant la direction Est/Ouest (+ pour l'Est et - pour l'Ouest)
    NS=oldpos[1] #position suivant la direction Nord/Sud (+ pour le Nord et - pour le Sud)
    f=int(oldpos[2]) #direction que regarde le ferry (de 0� � 360� dans le sens trigo avec 0� suivant l'Est)
    a=instr[0] #action
    v=int(instr[1:]) #valeur
    if a=="N":
        NS+=v
    elif a=="S":
        NS-=v
    elif a=="E":
        EW+=v
    elif a=="W":
        EW-=v
    elif a=="L":
        f+=v
    elif a=="R":
        f-=v
    elif a=="F":
        if f==0:
            EW+=v
        elif f==90:
            NS+=v
        elif f==180:
            EW-=v
        elif f==270:
            NS-=v
    else:
        print("Probl�me dans l'instruction")
    if f<0:
        f+=360
    elif f>=360:
        f-=360
    return [EW,NS,f]

def calc(l):
    oldpos=[0,0,0]
    for elt in l:
        newpos=calcpos(oldpos,elt)
        oldpos.clear()
        oldpos.extend(newpos)
        newpos.clear()
    return abs(oldpos[0])+abs(oldpos[1])
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration des groupes dans le fichier
    print(calc(liste))
    f.close()

main()
