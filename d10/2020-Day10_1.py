# -*-coding:Latin-1 -*
import os
    

def calc(l):
    lt=list(l)
    lt.sort()
    lt.insert(0,0) #ajout de la tention de la prise
    lt.append(lt[len(lt)-1]+3) #ajout de la tention de l'appareil
    #print(lt)
    i=1
    a=0 #pour compter les sauts de 1 jolt
    b=0 #pour compter les sauts de 3 jolts
    while i<len(lt):
        if lt[i]-lt[i-1]==1:
            a+=1
        elif lt[i]-lt[i-1]==3:
            b+=1
        else:
            print("il y a un problème")
        i+=1
    print("nb de sauts de 1 jolt :",a)
    print("nb de sauts de 3 jolts :",b)
    return a*b

def main():
    f=open('input.txt','r')
    #f=open('test2.txt','r')
    inp=f.read()
    liste=list(map(int,inp.split("\n"))) #récupération des groupes dans le fichier
    print(calc(liste))
    f.close()

main()
