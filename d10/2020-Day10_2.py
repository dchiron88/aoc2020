# -*-coding:Latin-1 -*
import os
    

def calc(l):
    lt=list(l)
    lt.sort()
    lt.insert(0,0) #ajout de la tention de la prise
    lt.append(lt[len(lt)-1]+3) #ajout de la tention de l'appareil
    #print(lt)
    i=1
    a=0 #pour compter les sauts de 1 jolt
    b=0 #pour compter les sauts de 3 jolts
    while i<len(lt):
        if lt[i]-lt[i-1]==1:
            a+=1
        elif lt[i]-lt[i-1]==3:
            b+=1
        else:
            print("il y a un probl�me")
        i+=1
    print("nb de sauts de 1 jolt :",a)
    print("nb de sauts de 3 jolts :",b)
    return a*b

def calc2(l):
    lt=list(l)
    lt.sort()
    lt.insert(0,0) #ajout de la tention de la prise
    lt.append(lt[len(lt)-1]+3) #ajout de la tention de l'appareil
    print(lt)
    i=0
    suc=[] #liste des pas de 1 qui se succ�dent
    while i<len(lt)-1:
        j=1
        if lt[i+1]-lt[i]==1:
            j=2
            while lt[i+j]-lt[i+j-1]==1: #ne devrait pas produire de boucles infinies car les listes se terminent par un step de 3
                j+=1
            suc.append(j)
            #print(j,":",lt[i:i+j])
        i+=j
    print(suc)
    s=1
    for elt in suc:
        s*=calc_possibilites(elt)
    return s

#fonction pour les calculs de possibilit�s en fonction du nombre d'adaptateurs qui se succ�dent avec un pas de 1 jolt
def calc_possibilites(n):
    s=[1,1,2]
    i=3
    while i<n:
        s.append(s[i-3]+s[i-2]+s[i-1])
        i+=1
    return s[n-1]

def main():
    f=open('input.txt','r')
    #f=open('test2.txt','r')
    inp=f.read()
    liste=list(map(int,inp.split("\n"))) #r�cup�ration des groupes dans le fichier
    #print(calc(liste))
    print("r�sultat de la partie 2 :",calc2(liste))
    f.close()

main()
