# -*-coding:Latin-1 -*
import os

f=open('2020-Day1_1 inp.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier
liste=[int(i) for i in liste] #conversion en int

for i, elt in enumerate(liste):
    j=i+1
    while j<len(liste):
        if elt+liste[j]==2020:
            print(str(elt) + " (position" + str(i) + ") et " + str(liste[j]) + " (position " + str(j) + ")")
            print(elt*liste[j])
        j+=1
f.close()

