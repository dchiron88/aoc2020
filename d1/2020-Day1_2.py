# -*-coding:Latin-1 -*
import os

f=open('2020-Day1_1 inp.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier
liste=[int(i) for i in liste] #conversion en int

for i, elt in enumerate(liste):
    j=i+1
    while j<len(liste):
        k=j+1
        while k<len(liste):
            if elt+liste[j]+liste[k]==2020:
                print(str(elt) + " (position" + str(i) + ") et " + str(liste[j]) + " (position " + str(j) + ") et " + str(liste[k]) + " (position " + str(k) + ")")
                print(elt*liste[j]*liste[k])
            k+=1
        j+=1
f.close()

