# -*-coding:Latin-1 -*
import os

def get_possible(fields):
    s=set()
    l=fields.split("\n")
    for elt in l:
        a=elt.split(": ")
        #print(a[1])
        b=a[1].split(" or ")
        #print(b[0],b[1])
        for elt2 in b:
            c=elt2.split("-")
            i=int(c[0])
            while i<=int(c[1]):
                s.add(i)
                i+=1
    return s
    
def calc(l):
    s=0
    pos=get_possible(l[0])
    #print(pos)
    tickets=l[2].split("\n")
    i=1
    while i<len(tickets):
        n=tickets[i].split(",")
        for j in n:
            if int(j) not in pos:
                s+=int(j)
        i+=1
    return s

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n\n") #récupération de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
