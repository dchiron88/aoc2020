# -*-coding:Latin-1 -*
import os

def get_possible(fields):
    s=set()
    l=fields.split("\n")
    for elt in l:
        a=elt.split(": ")
        b=a[1].split(" or ")
        #print(b[0],b[1])
        for elt2 in b:
            c=elt2.split("-")
            i=int(c[0])
            while i<=int(c[1]):
                s.add(i)
                i+=1
    return s
    
def get_list_epuree(l):
    pos=get_possible(l[0])
    #print(pos)
    tickets=l[2].split("\n")
    del tickets[0]
    i=0
    while i<len(tickets):
        n=tickets[i].split(",")
        for j in n:
            if int(j) not in pos:
                del tickets[i]
                i-=1
        i+=1
    return tickets

def get_dic(fields):
    l=fields.split("\n")
    d={}
    for elt in l:
        s=set()
        a=elt.split(": ")
        k=a[0]
        #print(k)
        b=a[1].split(" or ")
        for elt2 in b:
            c=elt2.split("-")
            i=int(c[0])
            while i<=int(c[1]):
                s.add(i)
                i+=1
        #print(s)
        d[k]=s
    return d

#Retourne une liste avec tous les n� des nearby tickets dans la colonne col
def get_col(tickets,col):
    l=[]
    i=0
    #print(tickets)
    #t=tickets.split("\n")
    while i<len(tickets):
        a=tickets[i].split(",")
        l.append(int(a[col]))
        i+=1
    return l

def is_field(d,l,champ):
    #print("d�but test")
    for elt in l:
        if int(elt) not in d[champ]:
            """print(elt,"n'est pas dans",champ)
            print(d[champ])"""
            return False
    return True

def fini(d):
    for val in d.values():
        if len(val)!=1:
            return False
    return True

def calc(l):
    s=1
    dic=get_dic(l[0]) #cl�: champs  |  val: valeurs possibles
    #print(dic)
    nb_fields=len(l[0].split("\n"))
    #Initialistaion du dictionnaire donnant les possibilit�s de champs associ�s aux colonnes
    i=0
    dt={} #cl� num�ro de colonne  |  val: champs possibles
    while i<nb_fields:
        dt[i]=[]
        li=get_list_epuree(l) #On vire les tickets foireux
        #print(li)
        col=get_col(li,i)
        """if i==0:
            print(col)"""
        for cle in dic.keys():
            #print(cle, col)
            if is_field(dic,col,cle):
                #print("le champ",cle,"fait partie de la colonne",i)
                dt[i].append(cle)
        i+=1
    #print(dt)
    #on �limine les possibilit�s
    while fini(dt)==False:
        for cle,val in dt.items():
            if len(val)==1:
                for cle2,val2 in dt.items():
                    if cle!=cle2 and val[0] in val2:
                        val2.remove(val[0])
    #print(dt)
    #calcul final
    myticket=l[1].split("\n")[1].split(",")
    #print(myticket)
    for cle,val in dt.items():
        print(cle,val[0])
        if val[0][:9]=="departure":
            s*=int(myticket[cle])
    return s

def main():
    f=open('input.txt','r')
    #f=open('test2.txt','r')
    inp=f.read()
    liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
