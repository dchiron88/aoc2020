# -*-coding:Latin-1 -*
import os

#retourne 1 si le si�ge est occup�, 0 sinon
def nb(l,li,co):
    if li<0 or li>=len(l) or co<0 or co>=len(l[li]):
        #print("on sort")
        return 0
    else:
        if l[li][co]=="#":
            return 1
        else:
            return 0
    
def compteoccupe(l,li,co):
    s=0
    #print("l[li][co] =",l[li][co],"(li =",li,"et co =",co,")")
    if l[li][co]!=".":
        s=nb(l,li-1,co-1)+nb(l,li,co-1)+nb(l,li+1,co-1)+nb(l,li+1,co)+nb(l,li+1,co+1)+nb(l,li,co+1)+nb(l,li-1,co+1)+nb(l,li-1,co)
    return s

def comptesiegesoccupes(l):
    n=0
    for elt in l:
        n+=elt.count("#")
    return n

def nextstep(l):
    lt=list(l)
    #print("lt =",*lt,sep="\n")
    i=0
    j=0
    tp=""
    while i<len(lt):
        j=0
        while j<len(lt[i]):
            c=compteoccupe(l,i,j)
            #print("l["+str(i)+"]["+str(j)+"] =",l[i][j],"| entour� de",c,"personnes")
            if l[i][j]=="L" and c==0:
                tp=lt[i][:j]+"#"+lt[i][j+1:]
                lt[i]=tp
                #print(*lt," ",sep="\n")
            elif l[i][j]=="#" and c>=4:
                tp=lt[i][:j]+"L"+lt[i][j+1:]
                lt[i]=tp
                #print(*lt," ",sep="\n")
            j+=1
        i+=1
    return lt

def calc(l):
    lo=l
    ln=nextstep(lo)
    #print("lo =",*lo, sep="\n")
    #print("ln =",*ln, sep="\n")
    while lo!=ln:
        #print("**************************** pour n =",n)
        lo.clear()
        lo.extend(ln)
        #print("lo =",*lo, sep="\n")
        ln.clear()
        ln.extend(nextstep(lo))
        #print("ln =",*ln, sep="\n")
    return comptesiegesoccupes(ln)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
