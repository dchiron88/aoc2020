# -*-coding:Latin-1 -*
import os

#retourne 1 si le si�ge est occup�, 0 s'il est libre et -1 si on sort de l'avion
def nb(l,li,co):
    if li<0 or li>=len(l) or co<0 or co>=len(l[li]):
        #print("on sort")
        return -1
    else:
        if l[li][co]=="#":
            return 1
        elif l[li][co]=="L":
            return 0

#Retourne 1 si le si�ge vu dans la direction � droite est occup�, 0 sinon
def n_d(l,li,co):
    co+=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co+=1
    return 0

#Retourne 1 si le si�ge vu dans la direction haut/droite est occup�, 0 sinon
def n_hd(l,li,co):
    co+=1
    li-=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co+=1
        li-=1
    return 0

#Retourne 1 si le si�ge vu dans la direction en haut est occup�, 0 sinon
def n_h(l,li,co):
    li-=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        li-=1
    return 0

#Retourne 1 si le si�ge vu dans la direction haut/gauche est occup�, 0 sinon
def n_hg(l,li,co):
    co-=1
    li-=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co-=1
        li-=1
    return 0

#Retourne 1 si le si�ge vu dans la direction � gauche est occup�, 0 sinon
def n_g(l,li,co):
    co-=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co-=1
    return 0

#Retourne 1 si le si�ge vu dans la direction bas/gauche est occup�, 0 sinon
def n_bg(l,li,co):
    co-=1
    li+=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co-=1
        li+=1
    return 0

#Retourne 1 si le si�ge vu dans la direction en bas est occup�, 0 sinon
def n_b(l,li,co):
    li+=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        li+=1
    return 0

#Retourne 1 si le si�ge vu dans la direction bas/droite est occup�, 0 sinon
def n_bd(l,li,co):
    co+=1
    li+=1
    while nb(l,li,co)!=-1:
        if nb(l,li,co)==1:
            return 1
        elif nb(l,li,co)==0:
            return 0
        co+=1
        li+=1
    return 0

def compteoccupe(l,li,co):
    s=0
    #print("l[li][co] =",l[li][co],"(li =",li,"et co =",co,")")
    if l[li][co]!=".":
        s=n_d(l,li,co)+n_hd(l,li,co)+n_h(l,li,co)+n_hg(l,li,co)+n_g(l,li,co)+n_bg(l,li,co)+n_b(l,li,co)+n_bd(l,li,co)
    return s

def comptesiegesoccupes(l):
    n=0
    for elt in l:
        n+=elt.count("#")
    return n

def nextstep(l):
    lt=list(l)
    #print("lt =",*lt,sep="\n")
    i=0
    j=0
    tp=""
    while i<len(lt):
        j=0
        while j<len(lt[i]):
            c=compteoccupe(l,i,j)
            #print("l["+str(i)+"]["+str(j)+"] =",l[i][j],"| entour� de",c,"personnes")
            if l[i][j]=="L" and c==0:
                tp=lt[i][:j]+"#"+lt[i][j+1:]
                lt[i]=tp
                #print(*lt," ",sep="\n")
            elif l[i][j]=="#" and c>=5:
                tp=lt[i][:j]+"L"+lt[i][j+1:]
                lt[i]=tp
                #print(*lt," ",sep="\n")
            j+=1
        i+=1
    return lt

def calc(l):
    lo=l
    ln=nextstep(lo)
    #print("lo =",*lo, sep="\n")
    #print("ln =",*ln, sep="\n")
    while lo!=ln:
        #print("**************************** pour n =",n)
        lo.clear()
        lo.extend(ln)
        #print("lo =",*lo, sep="\n")
        ln.clear()
        ln.extend(nextstep(lo))
        #print("ln =",*ln, sep="\n")
    return comptesiegesoccupes(ln)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
