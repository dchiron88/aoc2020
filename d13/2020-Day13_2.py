# -*-coding:Latin-1 -*
import os

def pgcd(a,b):
    while a!=b:
        if a>b:
            a-=b
        else:
            b-=a
    return a

def trouve_moi_une_congruence_a_X_connard(a,n,X):
    x=1
    while (x*a)%n!=X:
        x+=1
    return x*a

def calc2(l):
    IDs=l[1].split(",")
    i=0
    d={}
    n=1
    for elt in IDs:
        if elt!='x':
            d[int(elt)]=-i
            n*=int(elt)
        i+=1
    #print(d)
    #print(n)
    e={}
    for cle,val in d.items():
        t=trouve_moi_une_congruence_a_X_connard(int(n/cle),cle,1)
        e[cle]=t
    #print(e)
    prod=0
    for cle,val in d.items():
        prod+=val*e[cle]
    return prod%n
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération des groupes dans le fichier
    print(calc2(liste))
    f.close()

main()
