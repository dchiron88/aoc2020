# -*-coding:Latin-1 -*
import os
    
def calc(l):
    n=int(l[0]) #earliest timestamp you could depart on a bus
    IDs=l[1].split(",")
    a=n #nb de minutes mini avant le d�part d'un bus initialis� au max
    for elt in IDs:
        if elt!='x':
            i=int(elt)
            if i-n%i<a:
                a=i-n%i
                bus=i
    return a*bus
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration des groupes dans le fichier
    print(calc(liste))
    f.close()

main()
