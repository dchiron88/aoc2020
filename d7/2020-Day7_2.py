# -*-coding:Latin-1 -*
import os

def creerdic(l):
    dicos={}
    for elt in l:
        a=elt.replace(" bag","").replace(" bags","").replace(".","")
        a=a.split(" contain ")
        a[0]=a[0][:-1]
        b=a[1].split(", ")
        d={}
        for i in b:
            c=i.split(" ",1)
            if c[1][-1]=='s':
                c[1]=c[1][:-1]
            d[c[1]]=c[0]
        dicos[a[0]]=d
    return dicos

def calcsum(d):
    n=0
    for val in d.values():
        n+=int(val)
    return n
    
def newdic(olddic,dicos):
    dt={}
    print(olddic)
    for cle,val in olddic.items():
        for cle2, val2 in dicos[cle].items():
            if val2!='no':
                if cle2 in dt:
                    dt[cle2]+=int(val2)*int(val)
                else:
                    dt[cle2]=int(val2)*int(val)
    return dt

def calc(l):
    d={}
    dicos=creerdic(l)
    dep="shiny gold"
    #print(dicos)
    """for cle, val in dicos[dep].items():
        d[cle]=int(val)
    print(d)"""
    d=dicos[dep]
    n=calcsum(d)
    n0=0
    print(n)
    while n0!=n:
        n0=n
        dt=newdic(d,dicos)
        d.clear()
        d=dt
        n+=calcsum(d)
        print(n)
    return n
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    rules=inp.split("\n") #récupération des groupes dans le fichier
    f.close()
    print(calc(rules))
    
main()
