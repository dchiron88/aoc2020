# -*-coding:Latin-1 -*
import os

def creerdic(l):
    dicos={}
    for elt in l:
        a=elt.replace(" bag","").replace(" bags","").replace(".","")
        a=a.split(" contain ")
        a[0]=a[0][:-1]
        b=a[1].split(", ")
        d={}
        for i in b:
            c=i.split(" ",1)
            if c[1][-1]=='s':
                c[1]=c[1][:-1]
            d[c[1]]=c[0]
        dicos[a[0]]=d
    return dicos

def calc(l):
    res={'shiny gold'}
    taille=0
    while taille < len(res):
        taille=len(res)
        for cle, val in creerdic(l).items():
            for cle2 in val.keys():
                if cle2 in res:
                    res.add(cle)
    res.remove('shiny gold')
    #print(res)
    return len(res)
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    rules=inp.split("\n") #récupération des groupes dans le fichier
    f.close()
    print(calc(rules))
    
main()
