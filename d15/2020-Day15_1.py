# -*-coding:Latin-1 -*
import os

def next(dn2,dn1,last):
    lastval=last[1]
    lastturn=last[0]
    dt1=dict(dn1)
    if lastval not in dn2.keys():
        newval=0
    else:
        newval=lastturn-dn2[lastval]
    dt1[newval]=lastturn+1
    dt2=dict(dn1)
    return [dt2,dt1,lastturn+1,newval]
    
def calc(l,nb):
    last=[len(l),l[len(l)-1]] #[num�ro de tour,derni�re valeur]
    d={} #dictionnaire pour stocker les valeurs (key) et les tours (value)
    """for i, elt in enumerate(l):
        d[elt]=i+1"""
    i=0
    while i<len(l)-1:
        d[l[i]]=i+1
        i+=1
    dn2=dict(d) #dictionnaire au tour n-2
    d[l[-1]]=len(l)
    dn1=dict(d) #dictionnaire au tour n-1
    j=last[0]+1
    while j<=nb:
        #print("turn",j)
        t=next(dn2,dn1,last)
        dn2=t[0]
        dn1=t[1]
        last[0]=t[2]
        last[1]=t[3]
        j+=1
    return last[1]
	
	
def main():
    l=[0,3,6]
    nb=2020
    l=[11,0,1,10,5,19]
    print(calc(l,nb))
    
main()

