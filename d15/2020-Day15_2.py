# -*-coding:Latin-1 -*
import os

def addval(d,turn,lastval):
	if lastval not in d.keys():
		newval=0
	else:
		newval=turn-1-d[lastval]
	d[lastval]=turn-1
	return newval
    
def calc(l,nb):
    lastval=l[-1]
    d={} #dictionnaire pour stocker les valeurs (key) et les tours (value)
    i=0
    while i<len(l)-1:
        d[l[i]]=i+1
        i+=1
    j=len(l)+1
    while j<=nb:
        lastval=addval(d,j,lastval)
        j+=1
    return lastval
	
	
def main():
    l=[0,3,6]
    nb=2020
    print("r�sultat de l'exemple :",calc(l,nb))
    l=[11,0,1,10,5,19]
    nb=30000000
    print("part 1 :",calc(l,2020))
    print("part 2 :",calc(l,nb))
    
main()

