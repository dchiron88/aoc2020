# -*-coding:Latin-1 -*
import os

def oper(pos,l):
    r=[pos,0] #on stocke la nouvelle position puis la la valeur � ajouter l'accumulator
    t=l.split(" ")
    if t[0]=="acc":
        r[0]=pos+1
        r[1]=int(t[1])
    elif t[0]=="jmp":
        r[0]=pos+int(t[1])
    elif t[0]=="nop":
        r[0]=pos+1
    return r

#Fonction qui retourne True si �a boule et False sinon et la valeur acc
def testboucle(pos,instr):
    #b=[True,0]
    l=list(instr) #copie de la liste sinon �a garde toutes les modifs au fur et � mesure
    if l[pos][:3]=="jmp":
        l[pos]="nop"+l[pos][3:]
    elif l[pos][:3]=="nop":
        l[pos]="jmp"+l[pos][3:]
    else:
        return [True,0]
    #print(l)
    pos=0
    dejavu=[]
    acc=0
    stop=False
    while stop==False:
        dejavu.append(pos)
        #print(dejavu)
        t=oper(pos,l[pos])
        pos=t[0]
        acc+=t[1]
        if pos in dejavu:
            stop=True
            b=[True,0]
        if pos==len(l):
            stop=True
            b=[False,acc]
    return b

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    instr=inp.split("\n") #r�cup�ration des groupes dans le fichier
    i=0
    continuer=True
    while continuer:
        #print("test ligne",i)
        t=testboucle(i,instr)
        continuer=t[0]
        acc=t[1]
        i+=1
    print(acc)
    f.close()

main()
