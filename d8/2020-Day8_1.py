# -*-coding:Latin-1 -*
import os

def oper(pos,l):
    r=[pos,0] #on stocke la nouvelle position puis la la valeur � ajouter l'accumulator
    t=l.split(" ")
    if t[0]=="acc":
        r[0]=pos+1
        r[1]=int(t[1])
    elif t[0]=="jmp":
        r[0]=pos+int(t[1])
    elif t[0]=="nop":
        r[0]=pos+1
    return r
    
def calc(l):
    #temp=[]
    pos=0
    dejavu=[]
    acc=0
    while pos not in dejavu:
        dejavu.append(pos)
        t=oper(pos,l[pos])
        pos=t[0]
        acc+=t[1]
        #print(dejavu)
    return acc

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    instr=inp.split("\n") #r�cup�ration des groupes dans le fichier
    print(calc(instr))
    f.close()

main()
