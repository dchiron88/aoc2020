# -*-coding:Latin-1 -*
import os

def get_list(text,z,w):
    l=[]
    li=text.split("\n")
    i=len(li)-1
    while i>=0:
        j=0
        while j<len(li[i]):
            if li[i][j]=="#":
                l.append([j,len(li)-1-i,z,w])
            j+=1
        i-=1
    return l

def aff_list(l):
    i=get_zmin(l)
    zmax=get_zmax(l)
    wmin=get_wmin(l)
    wmax=get_wmax(l)
    while i<=zmax:
        j=wmin
        while j<wmax:
            print("\nz =",i,", w =",j)
            aff_lz(l,i,j)
            j+=1
        i+=1
    
def aff_lz(l,z,w):
    i=get_ymax(l)
    ymin=get_ymin(l)
    xmin=get_xmin(l)
    xmax=get_xmax(l)
    while i>=ymin:
        j=xmin
        lt=[]
        while j<=xmax:
            if [j,i,z,w] in l:
                lt.append("#")
            else:
                lt.append(".")
            j+=1
        print("".join(lt))
        i-=1
    
def get_xmin(l):
    m=l[0][0]
    for elt in l:
        if elt[0]<m:
            m=elt[0]
    return m

def get_xmax(l):
    m=l[0][0]
    for elt in l:
        if elt[0]>m:
            m=elt[0]
    return m

def get_ymin(l):
    m=l[0][1]
    for elt in l:
        if elt[1]<m:
            m=elt[1]
    return m

def get_ymax(l):
    m=l[0][1]
    for elt in l:
        if elt[1]>m:
            m=elt[1]
    return m

def get_zmin(l):
    m=l[0][2]
    for elt in l:
        if elt[2]<m:
            m=elt[2]
    return m

def get_zmax(l):
    m=l[0][2]
    for elt in l:
        if elt[2]>m:
            m=elt[2]
    return m

def get_wmin(l):
    m=l[0][3]
    for elt in l:
        if elt[3]<m:
            m=elt[3]
    return m

def get_wmax(l):
    m=l[0][3]
    for elt in l:
        if elt[3]>m:
            m=elt[3]
    return m

#Fonction pour convertir en base N
def baseN(n,N):
    q=-1
    res=''
    while q!=0:
        q=n//N
        r=n%N
        res=str(r)+res
        n=q
    while len(res)<4:
        res='0'+res
    return res

def get_neigh(coord):
    l=[]
    xc=coord[0]-1
    yc=coord[1]-1
    zc=coord[2]-1
    wc=coord[3]-1
    it=0
    while it<81:
        if it!=40: #pour ne pas retomber sur coord
            t3=baseN(it,3)
            l.append([xc+int(t3[0]),yc+int(t3[1]),zc+int(t3[2]),wc+int(t3[3])])
        it+=1
    return l

def count_neigh(l,coord):
    n=0
    ln=get_neigh(coord)
    for elt in ln:
        if elt in l:
            n+=1
    return n

def get_impacted(l):
    s=[]
    for elt in l:
        n=get_neigh(elt)
        for elt2 in n:
            if elt2 not in s:
                s.append(elt2)
    return s

def get_matrice(a,b,c,d):
    l=[]
    i=0
    while i<a:
        j=0
        while j<b:
            k=0
            while k<c:
                m=0
                while m<d:
                    l.append([i,j,k,m])
                    m+=1
                k+=1
            j+=1
        i+=1
    return l

def nextl(l):
    l0=list(l)
    xmin=get_xmin(l)
    xmax=get_xmax(l)
    ymin=get_ymin(l)
    ymax=get_ymax(l)
    zmin=get_zmin(l)
    zmax=get_zmax(l)
    wmin=get_wmin(l)
    wmax=get_wmax(l)
    DX=xmax-xmin+1
    DY=ymax-ymin+1
    DZ=zmax-zmin+1
    DW=wmax-wmin+1
    #m=max([DX,DY,DZ])+2
    #print("m =",m)
    mat=get_matrice(DX+2,DY+2,DZ+2,DW+2)
    m=len(mat)
    #print("mat=",mat)
    i=0
    while i<m:
        pour=i/m*100
        #print(int(pour*1000)/1000)
        if int(pour*100)%1000==0:
            print(int(pour),"% effectu�")
        ct=[xmin+mat[i][0]-1,ymin+mat[i][1]-1,zmin+mat[i][2]-1,wmin+mat[i][3]-1]
        nb=count_neigh(l0,ct)
        if ct in l0:
            if ct not in l:
                print(ct,"l� �a bug")
            if nb!=2 and nb!=3:
                l.remove(ct)
        else:
            if nb==3:
                l.append(ct)
        i+=1
    
def nextl2(l):
    l0=list(l)
    gi=get_impacted(l0)
    long=len(gi)
    i=0
    for elt in gi:
        pour=i/long*100
        i+=1
        if int(pour*100)%1000==0:
            print(int(pour),"% effectu�")
        nb=count_neigh(l0,elt)
        if elt in l0:
            if elt not in l:
                print(elt,"l� �a bug")
            if nb!=2 and nb!=3:
                l.remove(elt)
        else:
            if nb==3:
                l.append(elt)
    return len(l)

def calc(t):
    l0=get_list(t,0,0)
    #print(l0)
    #aff_list(l0)
    cycle=1
    while cycle<=6:
        print("\nafter",cycle,"cycle(s)")
        nextl(l0)
        #aff_list(l0)
        cycle+=1
    return len(l0)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    #liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier
    print(calc(inp))
    f.close()

main()
