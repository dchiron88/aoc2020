# -*-coding:Latin-1 -*
import os

def get_list(text,z):
    l=[]
    li=text.split("\n")
    i=len(li)-1
    while i>=0:
        j=0
        while j<len(li[i]):
            if li[i][j]=="#":
                l.append([j,len(li)-1-i,z])
            j+=1
        i-=1
    return l

def aff_list(l):
    i=get_zmin(l)
    zmax=get_zmax(l)
    while i<=zmax:
        print("z=",i)
        aff_lz(l,i)
        i+=1
    
def aff_lz(l,z):
    i=get_ymax(l)
    ymin=get_ymin(l)
    xmin=get_xmin(l)
    xmax=get_xmax(l)
    while i>=ymin:
        j=xmin
        lt=[]
        while j<=xmax:
            if [j,i,z] in l:
                lt.append("#")
            else:
                lt.append(".")
            j+=1
        print("".join(lt))
        i-=1
    
def get_xmin(l):
    m=l[0][0]
    for elt in l:
        if elt[0]<m:
            m=elt[0]
    return m

def get_xmax(l):
    m=l[0][0]
    for elt in l:
        if elt[0]>m:
            m=elt[0]
    return m

def get_ymin(l):
    m=l[0][1]
    for elt in l:
        if elt[1]<m:
            m=elt[1]
    return m

def get_ymax(l):
    m=l[0][1]
    for elt in l:
        if elt[1]>m:
            m=elt[1]
    return m

def get_zmin(l):
    m=l[0][2]
    for elt in l:
        if elt[2]<m:
            m=elt[2]
    return m

def get_zmax(l):
    m=l[0][2]
    for elt in l:
        if elt[2]>m:
            m=elt[2]
    return m

#Fonction pour convertir en base N
def baseN(n,N):
    q=-1
    res=''
    while q!=0:
        q=n//N
        r=n%N
        res=str(r)+res
        n=q
    while len(res)<3:
        res='0'+res
    return res

def get_neigh(coord):
    l=[]
    xc=coord[0]-1
    yc=coord[1]-1
    zc=coord[2]-1
    it=0
    while it<27:
        if it!=13: #pour ne pas retomber sur coord
            t3=baseN(it,3)
            l.append([xc+int(t3[0]),yc+int(t3[1]),zc+int(t3[2])])
        it+=1
    return l

def count_neigh(l,coord):
    n=0
    ln=get_neigh(coord)
    for elt in ln:
        if elt in l:
            n+=1
    return n

def get_matrice(a,b,c):
    l=[]
    i=0
    while i<a:
        j=0
        while j<b:
            k=0
            while k<c:
                l.append([i,j,k])
                k+=1
            j+=1
        i+=1
    return l
    
def nextl(l):
    l0=list(l)
    xmin=get_xmin(l)
    xmax=get_xmax(l)
    ymin=get_ymin(l)
    ymax=get_ymax(l)
    zmin=get_zmin(l)
    zmax=get_zmax(l)
    DX=xmax-xmin+1
    DY=ymax-ymin+1
    DZ=zmax-zmin+1
    #m=max([DX,DY,DZ])+2
    #print("m =",m)
    mat=get_matrice(DX+2,DY+2,DZ+2)
    #print("mat=",mat)
    i=0
    while i<len(mat):
        ct=[xmin+mat[i][0]-1,ymin+mat[i][1]-1,zmin+mat[i][2]-1]
        nb=count_neigh(l0,ct)
        if ct in l0:
            if ct not in l:
                print(ct,"l� �a bug")
            if nb!=2 and nb!=3:
                l.remove(ct)
        else:
            if nb==3:
                l.append(ct)
        i+=1

def calc(t):
    l0=get_list(t,0)
    #print(l0)
    #aff_list(l0)
    cycle=1
    while cycle<=6:
        #print("\nafter",cycle,"cycle(s)\n")
        nextl(l0)
        #aff_list(l0)
        cycle+=1
    return len(l0)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    #liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier
    print(calc(inp))
    f.close()

main()
