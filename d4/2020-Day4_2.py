# -*-coding:Latin-1 -*
import os

#v�rif si le champ est dans le passeport
def verif(champ, info):
    s=False
    fieldst=info.replace(" ", "\n")
    fields=fieldst.split("\n")
    #print("---",fields)
    for i, elt in enumerate(fields):
        temp=elt.split(":")
        #print(temp[0])
        if temp[0]==champ:
            a=temp[1]
            s = True
    #print(s)
    if s:
        if champ=="byr":
            if(verbyr(a)==False):
                s=False
        elif champ=="iyr":
            if(veriyr(a)==False):
                s=False
        elif champ=="eyr":
            if(vereyr(a)==False):
                s=False
        elif champ=="hgt":
            if(verhgt(a)==False):
                s=False
        elif champ=="hcl":
            if(verhcl(a)==False):
                s=False
        elif champ=="ecl":
            if(verecl(a)==False):
                s=False
        elif champ=="pid":
            if(verpid(a)==False):
                s=False
    return s

#v�rif si c'est un passeport
def isp(info):
    s = True
    champs=("byr","iyr","eyr","hgt","hcl","ecl","pid")
    for elt in champs:
        #print("******",elt)
        if verif(elt, info) == False:
            s = False
    return s

#fonction de v�rif de byr
def verbyr(a):
    s=False
    a=int(a)
    #print("***",a)
    if type(a)==int:
        if a>=1920 and a<=2002:
            s=True
    #print(s)
    return s

#fonction de v�rif de iyr
def veriyr(a):
    s=False
    a=int(a)
    #print("***",a)
    if type(a)==int:
        if a>=2010 and a<=2020:
            s=True
    #print(s)
    return s

#fonction de v�rif de eyr
def vereyr(a):
    s=False
    a=int(a)
    #print("***",a)
    if type(a)==int:
        if a>=2020 and a<=2030:
            s=True
    #print(s)
    return s
    
#fonction de v�rif de hgt
def verhgt(a):
    s=False
    #print("***",a)
    if a[-2:]=="cm":
        b=a[0:-2]
        b=int(b)
        if b>=150 and b<=193:
            s=True
    if a[-2:]=="in":
        b=a[0:-2]
        b=int(b)
        if b>=59 and b<=76:
            s=True
    #print(s)
    return s
#fonction de v�rif de hcl
def verhcl(a):
    s=False
    #print("***",a)
    if a[0]=="#" and len(a)==7:
        s=True
        i=1
        while i<7:
            if a[i] not in "0123456789abcdef":
                s=False
            i+=1
    #print(s)
    return s
    
#fonction de v�rif de ecl
def verecl(a):
    s=False
    #print("***",a)
    l=("amb","blu","brn","gry","grn","hzl","oth")
    for elt in l:
        if a==elt:
            s=True
    #print(s)
    return s
    
#fonction de v�rif de pid
def verpid(a):
    s=False
    #print("***",a)
    if len(a)==9:
        if (c.isdigit() for c in a):
            s=True
    #print(s)
    return s

f=open('input.txt','r')
inp=f.read()
liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier

n=0
for elt in liste:
    if isp(elt):
        n+=1
print(n)
"""print(liste[0])
print(isp(liste[0]))"""
f.close()
