# -*-coding:Latin-1 -*
import os

#v�rif si le champ est dans le passeport
def verif(champ, info):
    sortie=False
    fieldst=info.replace(" ", "\n")
    fields=fieldst.split("\n")
    #print("---",fields)
    for i, elt in enumerate(fields):
        temp=elt.split(":")
        #print(temp[0])
        if temp[0]==champ:
            sortie = True
    #print(sortie)
    return sortie

#v�rif si c'est un passeport
def isp(info):
    sortie = True
    champs=("byr","iyr","eyr","hgt","hcl","ecl","pid")
    for elt in champs:
        #print("******",elt)
        if verif(elt, info) == False:
            sortie = False
    return sortie

f=open('input.txt','r')
inp=f.read()
liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier

n=0
for elt in liste:
    if isp(elt):
        n+=1
print(n)
#print(liste[0])
#print(isp(liste[0]))
f.close()
