# -*-coding:Latin-1 -*
import os

def calc(ide):
    s=0
    FB=ide[:7]
    LR=ide[-3:]
    #print(FB,LR)
    FB=FB.replace("F","0")
    FB=FB.replace("B","1")
    FB="0b"+FB
    row=int(FB,2)
    #print("row =",row)
    LR=LR.replace("L","0")
    LR=LR.replace("R","1")
    LR="0b"+LR
    col=int(LR,2)
    #print("col =",col)
    s=row*8+col
    #print(s)
    return s


f=open('input.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier
res=0
for elt in liste:
    t=calc(elt)
    if t>res:
        res=t
print(res)
#print(calc("FBFBBFFRLR"))
f.close()

