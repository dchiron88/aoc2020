# -*-coding:Latin-1 -*
import os

def calc(ide):
    s=0
    FB=ide[:7]
    LR=ide[-3:]
    FB=FB.replace("F","0")
    FB=FB.replace("B","1")
    FB="0b"+FB
    row=int(FB,2)
    LR=LR.replace("L","0")
    LR=LR.replace("R","1")
    LR="0b"+LR
    col=int(LR,2)
    s=row*8+col
    return s

def calc2(ide):
    FB=ide[:7]
    LR=ide[-3:]
    FB=FB.replace("F","0")
    FB=FB.replace("B","1")
    FB="0b"+FB
    row=int(FB,2)
    LR=LR.replace("L","0")
    LR=LR.replace("R","1")
    LR="0b"+LR
    col=int(LR,2)
    s=str(row)+" "+str(col)
    return s

f=open('input.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier
plane=[]
i=0
while i<128:
    j=0
    while j<8:
        plane.append(str(i)+" "+str(j))
        j+=1
    i+=1
    
for elt in liste:
    plane.remove(calc2(elt))

IDs=[]
for elt in plane:
    a=elt.split(" ")
    row=a[0]
    col=a[1]
    IDs.append(int(row)*8+int(col))

for elt in IDs:
    if (elt-1 not in IDs) and (elt+1 not in IDs):
        print(elt)

#print(res)
#print(calc("FBFBBFFRLR"))
f.close()

