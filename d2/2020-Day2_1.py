# -*-coding:Latin-1 -*
import os

def ispwd(chaine):
    temp=chaine.split(" ")
    minmax=temp[0].split("-")
    mini=int(minmax[0])
    maxi=int(minmax[1])
    let=temp[1][0]
    chaine=temp[2]
    num=0
    for i in chaine:
        if i==let:
            num+=1;
    if num>=mini and num<=maxi:
        return True
    else:
        return False

f=open('2020-Day2inp.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier

correct=0
for i, elt in enumerate(liste):
    if(ispwd(elt)):
        correct+=1

print(correct)

f.close()

