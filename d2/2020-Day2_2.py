# -*-coding:Latin-1 -*
import os

def ispwd(chaine):
    temp=chaine.split(" ")
    minmax=temp[0].split("-")
    pos1=int(minmax[0])-1
    pos2=int(minmax[1])-1
    let=temp[1][0]
    chaine=temp[2]
    if (chaine[pos1]==let and chaine[pos2]!=let) or (chaine[pos1]!=let and chaine[pos2]==let):
        return True
    else:
        return False

f=open('2020-Day2inp.txt','r')
inp=f.read()
liste=inp.split("\n") #récupération de la liste dans le fichier

correct=0
for i, elt in enumerate(liste):
    if(ispwd(elt)):
        correct+=1

print(correct)

f.close()

