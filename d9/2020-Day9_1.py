# -*-coding:Latin-1 -*
import os
    
def verif(pos,l,n=25):
    b=False
    i=pos-n
    while i<pos-1:
        temp=list(l[pos-n:pos])
        temp.remove(l[i])
        if l[pos]-l[i] in temp:
            b=True
        i+=1
    return b

def calc(l,n=25):
    i=n
    s=0
    while i < len(l):
        if not verif(i,l,n):
            s=l[i]
        i+=1
    return s

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=list(map(int,inp.split("\n"))) #récupération des groupes dans le fichier
    #print(calc(liste,5))
    print(calc(liste))
    f.close()

main()
