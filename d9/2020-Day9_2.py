# -*-coding:Latin-1 -*
import os
    
def verif(pos,l,n=25):
    b=False
    i=pos-n
    #print(l[pos-n:pos])
    while i<pos-1:
        temp=list(l[pos-n:pos])
        temp.remove(l[i])
        if l[pos]-l[i] in temp:
            b=True
        i+=1
    return b

def calc(l,n=25):
    i=n
    s=0
    while i < len(l):
        #print(l[i])
        if not verif(i,l,n):
            s=l[i]
        i+=1
    return s

#fonction pour calculer la partie 2, a est le r�sultat de la partie 1
def calc2(l,a):
    i=0
    while i<len(l):
        s=l[i]
        j=1
        while s<a:
            s+=l[i+j]
            if s==a:
                deb=i
                fin=i+j
            j+=1
        i+=1
    lt=l[deb:fin+1]
    #print(lt)
    st=0
    for i in lt:
        st+=i
    #print("somme =",st,"deb =",deb,"fin =",fin)
    m=min(lt)
    M=max(lt)
    return m+M

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=list(map(int,inp.split("\n"))) #r�cup�ration des groupes dans le fichier
    #a=calc(liste,5)
    a=calc(liste)
    print("r�sultat part 1 :", a)
    print(calc2(liste,a))
    f.close()

main()
