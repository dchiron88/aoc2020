# -*-coding:Latin-1 -*
import os

#Convertit nb en binaire, retourne un string de k bits
def convertstrbit(nb,k):
    b=bin(int(nb))
    s=str(b[2:])
    while len(s)<k:
        s='0'+s
    return s

#application du masque sur nb
def applymask2(mask,nb):
    i=0
    while i<len(mask):
        if mask[i]!='0':
            nb=nb[:i]+mask[i]+nb[i+1:]
        i+=1
    return nb

#Donne la liste des adresses m�moire
def mem_adr(n):
    l=[]
    nbX=n.count('X')
    i=0
    while i<2**nbX:
        nbXstr=convertstrbit(i,nbX)
        j=len(n)-1
        k=len(nbXstr)-1
        nt=n
        while j>=0:
            if nt[j]=='X':
                nt=nt[:j]+nbXstr[k]+nt[j+1:]
                k-=1
            j-=1
        l.append(int(nt,2))
        i+=1
    return l

def calc(l):
    d={}
    for elt in l:
        if elt[:4]=='mask':
            m=elt[7:]
        else:
            lt=elt.split(' ')
            adress=lt[0][lt[0].find('[')+1:-1]
            val=int(lt[2]) #valeur � affecter aux diff�rentes adresses
            c=convertstrbit(adress,36)
            a=applymask2(m,c)
            ladress=mem_adr(a) #liste des adresses � �crire
            for i in ladress:
                d[i]=val
    s=0
    for val in d.values():
        s+=val
    return s

def main():
    f=open('input.txt','r')
    #f=open('test2.txt','r')
    inp=f.read()
    liste=inp.split("\n") #r�cup�ration de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
