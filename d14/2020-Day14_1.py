# -*-coding:Latin-1 -*
import os

def convertstrbit(nb):
    b=bin(int(nb))
    s=str(b[2:])
    while len(s)<36:
        s='0'+s
    return s

def applymask(mask,nb):
    i=0
    while i<len(mask):
        if mask[i]!='X':
            t=nb[:i]+mask[i]+nb[i+1:]
            nb=t
        i+=1
    return nb

def calc(l):
    d={}
    for elt in l:
        if elt[:4]=='mask':
            m=elt[7:]
        else:
            lt=elt.split(' ')
            adress=lt[0][lt[0].find('[')+1:-1]
            c=convertstrbit(lt[2])
            a=applymask(m,c)
            d[adress]=int(a,2)
    s=0
    for val in d.values():
        s+=val
    return s

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    print(calc(liste))
    f.close()

main()
