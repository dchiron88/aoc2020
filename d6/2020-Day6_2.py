# -*-coding:Latin-1 -*
import os

def calc(g):
    n=0
    liste=g.split("\n")
    for i in "abcdefghijklmnopqrstuvwxyz":
        temp=True
        for j in liste:
            if i not in j:
                temp=False
                break
        if temp:
            n+=1
    return n


f=open('input.txt','r')
#f=open('test.txt','r')
inp=f.read()
grp=inp.split("\n\n") #récupération des groupes dans le fichier
res=0
for elt in grp:
    res+=calc(elt)
    
print(res)
f.close()

