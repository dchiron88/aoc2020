# -*-coding:Latin-1 -*
import os

def calc(g):
    temp=[]
    for l in g:
        if l in "abcdefghijklmnopqrstuvwxyz":
            if l not in temp:
                temp.append(l)
    return len(temp)


f=open('input.txt','r')
#f=open('test.txt','r')
inp=f.read()
grp=inp.split("\n\n") #récupération des groupes dans le fichier
res=0
for elt in grp:
    res+=calc(elt)
    
print(res)
f.close()

